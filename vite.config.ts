import react from '@vitejs/plugin-react';
import {defineConfig} from 'vite';
import eslintPlugin from 'vite-plugin-eslint';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    {
      ...eslintPlugin({include: 'src/**/*.+(js|jsx|ts|tsx)'}),
      enforce: 'pre',
    },
    react(),
  ],
});
