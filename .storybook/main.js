module.exports = {
	stories: [
		'../design-system/**/src/components/**/*.stories.mdx',
		'../design-system/**/src/components/**/*.stories.@(ts|tsx)',
	],
	addons: [
		'@storybook/addon-links',
		'@storybook/addon-essentials',
		'@storybook/addon-interactions',
	],
	framework: '@storybook/react',
	core: {
		builder: '@storybook/builder-vite',
	},
};
