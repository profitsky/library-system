import { ThemeProvider as StyledComponentThemeProvider } from 'styled-components';
import { createTheme, ThemeProvider as MaterialUiThemeProvider } from '@mui/material';
import { themesPromy24 } from '../design-system/promy24/src/config/theme/themesPromy24';
import { themesPlatformaPrzewoznika } from '../design-system/platformaPrzewoznika/src/config/theme/themesPlatformaPrzewoznika';
import { promy24GlobalStyleObject } from '../design-system/promy24/src/config/theme/promy24GlobalStyle';
import { platformaPrzewoznikaGlobalStyleObject } from '../design-system/platformaPrzewoznika/src/config/theme/platformaPrzewoznikaGlobalStyle';

const themesArray = [
	themesPromy24.lightThemePromy24,
	themesPromy24.darkThemePromy24,
	themesPlatformaPrzewoznika.lightThemePlatformaPrzewoznika,
	themesPlatformaPrzewoznika.darkThemePlatformaPrzewoznika,
];

const globalStylesArray = [promy24GlobalStyleObject, platformaPrzewoznikaGlobalStyleObject];

const generateItems = () => {
	const temporaryArray = [];
	for (const { title, value, icon } of themesArray) {
		temporaryArray.push({
			title,
			value,
			icon,
		});
	}
	return temporaryArray;
};

export const parameters = {
	actions: { argTypesRegex: '^on[A-Z].*' },
};

export const globalTypes = {
	theme: {
		name: 'Theme',
		description: 'Globalny motyw',
		defaultValue: themesPromy24.lightThemePromy24.value,
		toolbar: {
			items: generateItems(),
			showName: true,
			dynamicTitle: true,
		},
	},
};

const getTheme = (themeName) => {
	return themesArray.find(({ value }) => value === themeName);
};

const getGlobalStyle = (themeName) => {
	return globalStylesArray
		.filter(({ theme, globStyle }) => {
			return themeName.includes(theme);
		})
		.map(({ globStyle, theme }) => {
			const GlobalStyle = globStyle;
			return <GlobalStyle key={theme} />;
		});
};

const withThemeProvider = (Story, context) => {
	const theme = getTheme(context.globals.theme);

	return (
		<MaterialUiThemeProvider theme={createTheme(theme)}>
			<StyledComponentThemeProvider theme={theme}>
				<Story {...context} />
				{getGlobalStyle(context.globals.theme)}
			</StyledComponentThemeProvider>
		</MaterialUiThemeProvider>
	);
};
export const decorators = [withThemeProvider];
