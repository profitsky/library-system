import React from 'react';

interface IButton {
	children: React.ReactNode;
}

export const ButtonCds = ({ children }: IButton) => <div>{children}</div>;
