import react from '@vitejs/plugin-react';
import * as path from 'path';
import {defineConfig} from 'vite';
import dts from 'vite-plugin-dts';
import eslintPlugin from 'vite-plugin-eslint';

// https://vitejs.dev/config/
export default defineConfig({
	plugins: [
		{
			...eslintPlugin({ include: 'src/**/*.+(js|jsx|ts|tsx)' }),
			enforce: 'pre',
		},
		react(),
		dts({
			insertTypesEntry: true,
		}),
	],
	build: {
		sourcemap: true,
		lib: {
			entry: path.resolve(__dirname, 'src/index.ts'),
			name: 'promy24',
			formats: ['es', 'umd'],
			fileName: (format) => `promy24.${format}.js`,
		},
		outDir: './dist',
		rollupOptions: {
			external: [
				'react',
				'react-dom',
				'styled-components',
				'@mui/material',
				'@emotion/react',
				'@emotion/styled',
			],
			output: {
				globals: {
					react: 'React',
					'react-dom': 'ReactDOM',
					'styled-components': 'styled',
					'@mui/material': '@mui/material',
					'@emotion/react': '@emotion/react',
					'@emotion/styled': '@emotion/styled',
				},
			},
		},
	},
	server: {
		watch: {
			ignored: ['!**/.idea/**'],
		},
	},
});
