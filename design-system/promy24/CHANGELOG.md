# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# 0.1.0 (2022-07-12)


### Bug Fixes

* fix path to CI ([14351dc](https://gitlab.oliwa.pks-sa.com/codiway/frontend/storybooks/codiway-design-system/commit/14351dcfcb36d31271fd4969e3b8582248aeb461))
* revert versions && lerna build on CI ([cf3c941](https://gitlab.oliwa.pks-sa.com/codiway/frontend/storybooks/codiway-design-system/commit/cf3c941e1fe19b534316c148b4b0a708f4315da3))


### Features

* [#2](https://gitlab.oliwa.pks-sa.com/codiway/frontend/storybooks/codiway-design-system/issues/2)kppx53/Merge_repozytorium_testowego_do_Codiway_Design_System ([b327264](https://gitlab.oliwa.pks-sa.com/codiway/frontend/storybooks/codiway-design-system/commit/b327264368b603c7755d0081a4d0d19055271d30)), closes [#2kppx53](https://gitlab.oliwa.pks-sa.com/codiway/frontend/storybooks/codiway-design-system/issues/2kppx53)
