export const textStylesPromy24 = {
	headlines: {
		h1: {
			fontSize: '16px',
			lineHeight: '24px',
			fontWeight: 700,
			letterSpacing: '2%',
		},
		h2: {
			fontSize: '16px',
			lineHeight: '24px',
			fontWeight: 700,
			letterSpacing: '2%',
		},
		h3: {
			fontSize: '16px',
			lineHeight: '24px',
			fontWeight: 700,
			letterSpacing: '2%',
		},
		h4: {
			fontSize: '16px',
			lineHeight: '24px',
			fontWeight: 700,
			letterSpacing: '2%',
		},
	},
	bodyText: {
		body1: {
			semiBold: {
				fontSize: '16px',
				lineHeight: '24px',
				fontWeight: 700,
				letterSpacing: '2%',
			},
			regular: {
				fontSize: '16px',
				lineHeight: '24px',
				letterSpacing: '2%',
			},
		},
		body2: {
			semiBold: {
				fontSize: '16px',
				lineHeight: '24px',
				fontWeight: 700,
				letterSpacing: '2%',
			},
			regular: {
				fontSize: '16px',
				lineHeight: '24px',
				letterSpacing: '2%',
			},
		},
	},
};
