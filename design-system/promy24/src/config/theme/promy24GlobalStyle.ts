import { createGlobalStyle } from 'styled-components';

export const Promy24GlobalStyle = createGlobalStyle`
  ${({ theme }) => `
      html {
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
      }

      *,
      *:before,
      *:after {
        -webkit-box-sizing: inherit;
        -moz-box-sizing: inherit;
        box-sizing: inherit;
      }

      body {
        margin: 0;
        font-family: sans-serif;
        font-size: 62,5%;
        background-color: ${theme.background.light};
      }
  `}
`;

export const promy24GlobalStyleObject = {
	theme: 'Promy24',
	globStyle: Promy24GlobalStyle,
};
