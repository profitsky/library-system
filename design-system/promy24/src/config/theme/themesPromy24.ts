import { darkThemePromy24 } from './darkThemePromy24';
import { lightThemePromy24 } from './lightThemePromy24';

export type ThemeType = typeof lightThemePromy24;

export const themesPromy24 = {
  lightThemePromy24,
  darkThemePromy24,

};
