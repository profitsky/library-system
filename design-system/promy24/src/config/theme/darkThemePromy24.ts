import { commonThemePromy24 } from './commonThemePromy24';
import { textStylesPromy24 } from './textStylesPromy24';

export const darkThemePromy24 = {
	title: 'Promy 24 Dark',
	value: 'darkThemePromy24',
	icon: 'circle',
	background: {
		light: '#282828',
		darken: '#1C1C1C',
	},
	greyScale: {
		text: {
			primary: '#D3D7E7',
			secondary: '#9D9D9D',
			tertiary: '#74829C',
		},
		stroke: '#3A3A3A',
		dividers: '#323232',
		disabled: {
			element: '#3D3D3D',
			text: '#525E74',
			background: '#4A4A4A',
		},
		white: '#FFFFFF',
	},
	accentBlue: commonThemePromy24.accentBlue,
	systemColors: commonThemePromy24.systemColors,
	space: commonThemePromy24.space,
	textStyles: { textStylesPromy24 },
};
