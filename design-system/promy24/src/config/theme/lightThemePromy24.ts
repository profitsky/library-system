import { commonThemePromy24 } from './commonThemePromy24';
import { textStylesPromy24 } from './textStylesPromy24';

export const lightThemePromy24 = {
	title: 'Promy 24 Light',
	value: 'lightThemePromy24',
	icon: 'circlehollow',
	background: {
		light: '#FFFFFF',
		darken: '#F4F6FA',
	},
	greyScale: {
		text: {
			primary: '#23262F',
			secondary: '#3D456C',
			tertiary: '#74829C',
		},
		stroke: '#E9EDF3',
		dividers: '#E3E9F6',
		disabled: {
			element: '#C2CBDE',
			text: '#8B93AC',
			background: '#F4F6FA',
		},
		white: '#FFFFFF',
	},
	accentBlue: commonThemePromy24.accentBlue,
	systemColors: commonThemePromy24.systemColors,
	space: commonThemePromy24.space,
	textStyles: { textStylesPromy24 },
};
