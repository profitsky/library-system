import { ThemeType } from '../theme/themesPromy24';

declare module 'styled-components' {
	export interface DefaultTheme extends ThemeType {}
}
