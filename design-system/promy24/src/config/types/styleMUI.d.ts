import { ThemeType } from '../theme/themesPromy24';

declare module '@mui/material/styles' {
	interface Theme extends ThemeType {}

	// allow configuration using `createTheme`
	interface ThemeOptions extends ThemeType {}
}
