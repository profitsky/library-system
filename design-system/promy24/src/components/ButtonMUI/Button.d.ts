declare global {
  declare module '@mui/material/Button' {
    interface ButtonPropsVariantOverrides {
      primary: true;
      secondary: true;
      tertiary: true;
      outlined: false;
      text: false;
      contained: false;
    }

    interface ButtonPropsSizeOverrides {
      XS: true;
      S: true;
      M: true;
      L: true;
      small: false;
      medium: false;
      large: false;
    }
  }
}
