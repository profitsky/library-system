import React, { ButtonHTMLAttributes } from 'react';

export type ButtonMUIVariantType = 'primary' | 'secondary' | 'tertiary';
export type ButtonMUISizeType = 'XS' | 'S' | 'M' | 'L';

export interface IButtonMUI extends ButtonHTMLAttributes<HTMLButtonElement> {
	children?: React.ReactNode;
	variant?: ButtonMUIVariantType;
	onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
	size?: ButtonMUISizeType;
}
