import { styled, Theme } from '@mui/material';
import Button from '@mui/material/Button';

import { ButtonMUISizeType, ButtonMUIVariantType } from './ButtonMUI.types';

interface IStyledButtonMUI {
	variant: ButtonMUIVariantType;
	size: ButtonMUISizeType;
}

const heightSizeMap = (theme: Theme, size: IStyledButtonMUI['size']) => {
	const height = {
		XS: `${theme.space[6]}px`,
		S: `${theme.space[8]}px`,
		M: `${theme.space[10]}px`,
		L: `${theme.space[12]}px`,
	};
	return height[size];
};

const textColorVariantMap = (theme: Theme, variant: ButtonMUIVariantType) => {
	const variants = {
		primary: theme.greyScale.white,
		secondary: theme.greyScale.text.secondary,
		tertiary: theme.greyScale.text.secondary,
	};
	return variants[variant];
};

const backgroundColorVariantMap = (theme: Theme, variant: ButtonMUIVariantType) => {
	const variants = {
		primary: theme.accentBlue.accent1.primary,
		secondary: theme.greyScale.white,
		tertiary: 'transparent',
	};
	return variants[variant];
};

const hoverBackgroundVariantMap = (theme: Theme, variant: ButtonMUIVariantType) => {
	const variants = {
		primary: theme.accentBlue.state.hover,
		secondary: theme.background.darken,
		tertiary: 'transparent',
	};
	return variants[variant];
};

const actionBackgroundVariantMap = (theme: Theme, variant: ButtonMUIVariantType) => {
	const variants = {
		primary: theme.accentBlue.state.click,
		secondary: theme.background.darken,
		tertiary: 'transparent',
	};
	return variants[variant];
};

const focusBackgroundVariantMap = (theme: Theme, variant: ButtonMUIVariantType) => {
	const variants = {
		primary: theme.accentBlue.accent1.primary,
		secondary: theme.greyScale.white,
		tertiary: 'transparent',
	};
	return variants[variant];
};

const disabledBackgroundVariantMap = (theme: Theme, variant: ButtonMUIVariantType) => {
	const variants = {
		primary: theme.background.darken,
		secondary: theme.background.darken,
		tertiary: 'transparent',
	};
	return variants[variant];
};

export const StyledButtonMUI = styled(Button)<IStyledButtonMUI>`
	${({ variant, size, theme }) => `
	    font-size: 15px;
	    border-radius: ${theme.space[6]}px;
	    height: ${heightSizeMap(theme, size)};
      color: ${textColorVariantMap(theme, variant)};
      background-color: ${backgroundColorVariantMap(theme, variant)};
        &:hover {
            background-color: ${hoverBackgroundVariantMap(theme, variant)};
          }
        &:active {
            background-color: ${actionBackgroundVariantMap(theme, variant)};
          }
        &:focus {
            background-color: ${focusBackgroundVariantMap(theme, variant)};
          }
        &:disabled {
           color: ${theme.greyScale.disabled.text};
           background-color: ${disabledBackgroundVariantMap(theme, variant)};
          }
	`}
`;
