import React from 'react';

import { StyledButtonMUI } from './ButtonMUI.styled';
import { IButtonMUI } from './ButtonMUI.types';

export const ButtonMUI = ({ variant = 'primary', size = 'XS', children, onClick }: IButtonMUI) => (
	<StyledButtonMUI variant={variant} onClick={onClick} size={size} disableElevation disableRipple>
		{children}
	</StyledButtonMUI>
);
