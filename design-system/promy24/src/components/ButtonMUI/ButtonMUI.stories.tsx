import { Meta, Story } from '@storybook/react';
import React from 'react';

import { ButtonMUI } from './ButtonMUI';
import { IButtonMUI } from './ButtonMUI.types';

export default {
	title: 'Promy24/ButtonMUI',
	component: ButtonMUI,
} as Meta;

const Template: Story<IButtonMUI> = (args: JSX.IntrinsicAttributes & IButtonMUI) => (
	<ButtonMUI {...args} />
);

export const Primary = Template.bind({});
Primary.args = {
	variant: 'primary',
	children: 'Primary',
};

export const Secondary = Template.bind({});
Secondary.args = {
	variant: 'secondary',
	children: 'Secondary',
};

export const Tertiary = Template.bind({});
Tertiary.args = {
	variant: 'tertiary',
	children: 'Tertiary',
};
