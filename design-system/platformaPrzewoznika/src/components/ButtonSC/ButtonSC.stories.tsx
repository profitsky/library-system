import { Meta, Story } from '@storybook/react';
import React from 'react';

import { ButtonSC, IButton } from './ButtonSC';

export default {
	title: 'PlatformaPrzewoznika/Button',
	component: ButtonSC,
} as Meta;

const Template: Story<IButton> = (args: JSX.IntrinsicAttributes & IButton) => (
	<ButtonSC {...args} />
);

export const Primary = Template.bind({});
Primary.args = {
	variant: 'primary',
	children: 'siema',
};
