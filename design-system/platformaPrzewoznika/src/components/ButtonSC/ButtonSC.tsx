import React, { ButtonHTMLAttributes } from 'react';

import { ButtonVariantType, StyledButton } from './ButtonSC.styled';

export interface IButton extends ButtonHTMLAttributes<HTMLButtonElement> {
	children?: React.ReactNode;
	variant?: ButtonVariantType;
	onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
}

export const ButtonSC = ({ children, onClick, variant = 'primary' }: IButton) => (
	<StyledButton type="button" variant={variant} onClick={onClick}>
		{children}
	</StyledButton>
);
