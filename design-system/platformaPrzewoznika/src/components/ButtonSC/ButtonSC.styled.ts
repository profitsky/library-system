import styled from 'styled-components';

export type ButtonVariantType = 'primary' | 'secondary';

interface IStyledButton {
	variant: ButtonVariantType;
}

export const StyledButton = styled.button<IStyledButton>`
	${({ theme }) => `
	  border: none;
    color:  yellow;
    padding: 15px 32px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    border-radius: ${theme.background.light};
    cursor: pointer;
    background-color: black;
	`}
`;
