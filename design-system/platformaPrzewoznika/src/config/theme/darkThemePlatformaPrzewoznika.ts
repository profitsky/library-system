import { commonThemePlatformaPrzewoznika } from './commonThemePlatformaPrzewoznika';
import { textStylesPlatformaPrzewoznika } from './textStylesPlatformaPrzewoznika';

export const darkThemePlatformaPrzewoznika = {
	title: 'Platforma Przewoznika Dark',
	value: 'darkThemePlatformaPrzewoznika',
	icon: 'circle',
	background: {
		light: '#282828',
		darken: '#1C1C1C',
	},
	greyScale: {
		text: {
			primary: '#D3D7E7',
			secondary: '#9D9D9D',
			tertiary: '#74829C',
		},
		stroke: '#3A3A3A',
		dividers: '#323232',
		disabled: {
			element: '#3D3D3D',
			text: '#525E74',
			background: '#4A4A4A',
		},
		white: '#FFFFFF',
	},
	accentBlue: commonThemePlatformaPrzewoznika.accentBlue,
	systemColors: commonThemePlatformaPrzewoznika.systemColors,
	space: commonThemePlatformaPrzewoznika.space,
	textStyles: { textStylesPlatformaPrzewoznika },
};
