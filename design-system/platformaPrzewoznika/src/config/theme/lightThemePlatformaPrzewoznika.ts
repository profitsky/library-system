import { commonThemePlatformaPrzewoznika } from './commonThemePlatformaPrzewoznika';
import { textStylesPlatformaPrzewoznika } from './textStylesPlatformaPrzewoznika';

export const lightThemePlatformaPrzewoznika = {
	title: 'Platforma Przewoznika Light',
	value: 'lightThemePlatformaPrzewoznika',
	icon: 'circlehollow',
	background: {
		light: '#FFFFFF',
		darken: '#F4F6FA',
	},
	greyScale: {
		text: {
			primary: '#23262F',
			secondary: '#3D456C',
			tertiary: '#74829C',
		},
		stroke: '#E9EDF3',
		dividers: '#E3E9F6',
		disabled: {
			element: '#C2CBDE',
			text: '#8B93AC',
			background: '#F4F6FA',
		},
		white: '#FFFFFF',
	},
	accentBlue: commonThemePlatformaPrzewoznika.accentBlue,
	systemColors: commonThemePlatformaPrzewoznika.systemColors,
	space: commonThemePlatformaPrzewoznika.space,
	textStyles: { textStylesPlatformaPrzewoznika },
};
