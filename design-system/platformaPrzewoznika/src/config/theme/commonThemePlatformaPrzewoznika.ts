export const commonThemePlatformaPrzewoznika = {
	accentBlue: {
		accent1: {
			primary: '#F8F9FF',
			secondary: '#EDEBFF',
			tertiary: '#B4AEFF',
			quaternary: '#6147FF',
		},
		state: {
			hover: '#0C008F',
			click: '#3022CD',
		},
	},
	systemColors: {
		successful: {
			primary: '#29A335',
			secondary: '#E4FFE7',
			tertiary: '#ADFFB5',
			hover: '#56DA63',
		},
		attention: {
			primary: '#AC890C',
			secondary: '#FFF8DD',
			tertiary: '#FFE794',
			hover: '#AC890C',
		},
		error: {
			primary: '#ED2121',
			secondary: '#FFEAEA',
			tertiary: '#FFC0C0',
			hover: '#FF5252',
		},
	},
	space: new Array(35).fill(null).map((value, index) => index * 4),
};
