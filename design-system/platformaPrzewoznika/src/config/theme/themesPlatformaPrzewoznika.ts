import { darkThemePlatformaPrzewoznika } from './darkThemePlatformaPrzewoznika';
import { lightThemePlatformaPrzewoznika } from './lightThemePlatformaPrzewoznika';

export type ThemeType = typeof lightThemePlatformaPrzewoznika;

export const themesPlatformaPrzewoznika = {
  lightThemePlatformaPrzewoznika,
  darkThemePlatformaPrzewoznika,
};
