import { ThemeType } from '../theme/themesPlatformaPrzewoznika';

declare module 'styled-components' {
	export interface DefaultTheme extends ThemeType {}
}
