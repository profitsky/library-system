import { ThemeType } from '../theme/themesPlatformaPrzewoznika';

declare module '@mui/material/styles' {
	interface Theme extends ThemeType {}

	// allow configuration using `createTheme`
	interface ThemeOptions extends ThemeType {}
}
